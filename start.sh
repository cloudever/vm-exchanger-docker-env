#!/usr/bin/sh

if [ ! -f ./config.env ]; then
  echo "config.env not found!"
  exit 1
fi

# Create app dir
if [ ! -d ./html/public ]; then
  echo "./public/html does not exists, creating.."
  mkdir -p ./html/public
fi

# Create MySQL data dir
mkdir -p ./data/mysql

docker-compose up -f docker-compose.yml