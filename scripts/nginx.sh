#!/bin/sh

envsubst '$$HOST' < /etc/nginx/conf.d/default.template > /etc/nginx/conf.d/default.conf
envsubst '$$HOST' < /etc/nginx/conf.d/phpmyadmin.template > /etc/nginx/conf.d/phpmyadmin.conf

nginx -g 'daemon off;'